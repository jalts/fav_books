from __future__ import unicode_literals
from django.db import models
import re

class UserManager(models.Manager):
    def create_validator(self, reqPOST):
        errors = {}
        EMAIL_REGEX = re.compile(r'^[a-zA-Z0-9.+_-]+@[a-zA-Z0-9._-]+\.[a-zA-Z]+$')
        if len(reqPOST['first_name']) < 2:
            errors['first_name'] = "first name must be at least 2 chars"
        if len(reqPOST['last_name']) < 2:
            errors['last_name'] = "first name must be at least 2 chars"
        if not EMAIL_REGEX.match(reqPOST['email']):
            errors['email'] = "email must be valid format"
        if len(reqPOST['password']) < 6:
            errors['password'] = 'password must be at least 8 chars'
        if reqPOST['password'] != reqPOST['confirm_password']:
            errors['confirm_password'] = "passwords do not match"
        return errors


class User(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    email = models.CharField(max_length=100)
    password = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = UserManager()

class BookManager(models.Manager):
    def create_validator(self, reqPOST):
        errors = {}
        if len(reqPOST['title']) < 2:
            errors['title'] = "book title is too short"
        if len(reqPOST['description']) < 2:
            errors['description'] = "description is too short"
        books_with_same_name = Book.objects.filter(title=reqPOST['title'])
        if len(books_with_same_name) > 0:
            errors['duplicate'] = "That name is already taken!"
        return errors

class Book(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField()
    user = models.ForeignKey(User, related_name="books_owned", on_delete=models.CASCADE)
    favbook = models.ManyToManyField(User, related_name="faved_book")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = BookManager()

""" class Book(models.Model):
    title = models.CharField(max_length=255)
    description = models.CharField(max_length=255)
    creator = models.ForeignKey(User, related_name="has_created_books", on_delete=models.CASCADE)
    favorited_by = models.ManyToManyField(User, related_name="favorited_books")
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True) """
