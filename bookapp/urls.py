from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.index),
    path('register', views.register),
    path('login', views.login),
    path('main_page', views.main_page),
    path('logout', views.logout),
    path('add_book', views.add_book),
    path('edit_book_page/<int:id>', views.edit_book_page),
    path('books/favorite/<int:id>', views.fav_book),
    path('books/un_fav/<int:id>', views.unfav_book),
    path('edit/<int:id>', views.edit_book),
]


