from django.shortcuts import render, HttpResponse, redirect
from django.contrib import messages
from .models import *
import bcrypt

def index(request):
    # return HttpResponse("you did")
    return render(request, "index.html")

def register(request):
    if request.method == "POST":
        errors = User.objects.create_validator(request.POST)
        if len(errors) > 0:
            for key, value in errors.items():
                messages.error(request, value)
            return redirect('/')
        else:
            hashed_pw = bcrypt.hashpw(request.POST['password'].encode(), bcrypt.gensalt()).decode()
            print(hashed_pw)
            user = User.objects.create(first_name=request.POST['first_name'], last_name=request.POST['last_name'], email=request.POST['email'], password=hashed_pw)
            request.session['user_id'] = user.id
            return redirect('/main_page')
    return redirect('/')

def login(request):
    user = User.objects.filter(email=request.POST['email'])
    if len(user) > 0:
        user = user[0]
        if bcrypt.checkpw(request.POST['password'].encode(), user.password.encode()):
            request.session['user_id'] = user.id
            return redirect('/main_page')
    messages.error(request, "email or pass is incorrect")
    return redirect('/')

def main_page(request):
    if 'user_id' not in request.session:
        messages.error(request, "you need to register or login!")
        return redirect('/')
    context = {
        'user': User.objects.get(id=request.session['user_id']),
        'all_books': Book.objects.all()
    }
    return render(request, "main_page.html", context)

def logout(request):
    request.session.clear()
    return redirect('/')

def add_book(request):
    if 'user_id' not in request.session:
        return redirect('/')
    if request.method == "POST":
        errors = Book.objects.create_validator(request.POST)
        if len(errors) > 0:
            for key, value in errors.items():
                messages.error(request, value)
            return redirect('/main_page')
        else:
            new_book = Book.objects.create(title=request.POST['title'], description=request.POST['description'], user=User.objects.get(id=request.session['user_id']))
        return redirect('/main_page')
    return redirect('/main_page')

def edit_book_page(request, id):
    if 'user_id' not in request.session:
        messages.error(request, "you need to register or login!")
        return redirect('/')
    book_with_id = Book.objects.filter(id=id)
    if len(book_with_id) > 0:
        context = {
            'user': User.objects.get(id=request.session['user_id']),
            'book': Book.objects.get(id=id),
        }
        return render(request, "edit_book.html", context)
    else:
        messages.error(request, "book not found.")
        return redirect('/main_page')

def fav_book(request, id):
    if 'user_id' not in request.session:
        return redirect('/')
    if request.method == "POST":
        book_with_id = Book.objects.filter(id=id)
        if len(book_with_id) > 0:
            book = Book.objects.get(id=id)
            user = User.objects.get(id=request.session['user_id'])
            book.favbook.add(user)
            #user.voted_koalas.add(koala)
    return redirect('/main_page')

def unfav_book(request, id):
    if 'user_id' not in request.session:  #checking to see if someone is really using the application
        return redirect('/')
    if request.method == "POST":  #checking to see if this is a POST method 
        book_with_id = Book.objects.filter(id=id)  #checking to make sure we are filtering within our app - is the ID ok?
        if len(book_with_id) > 0:
            book = Book.objects.get(id=id)
            user = User.objects.get(id=request.session['user_id'])
            book.favbook.remove(user)
            #user.voted_koalas.add(koala)
    return redirect('/main_page')

def edit_book(request, id):
    if request.method=='POST':
        my_book = Book.objects.get(id=id)
        my_book.title=request.POST['new_title']
        my_book.description=request.POST['new_movie_description']
        my_book.save()
        return redirect(f"/edit_book_page/{my_book.id}")
    return redirect('/')